

#Usando una oficial imagen java
FROM openjdk:12-jdk-alpine3.9

# Set the working directory to /app
#WORKDIR /app

# Copy the current directory contents into the container at /app
#ADD . /app

RUN mkdir -p /usr/local/user-backend-management-with-admin

ARG JAR_FILE
#Nombrando un alias para referirme luego en el run.sh
ADD ${JAR_FILE} /usr/local/user-backend-management-with-admin/app-service.jar


# Make port 80 available to the world outside this container
EXPOSE 8080

# Define environment variable
ENV NAME Nelson
ENV server-port 8080



ADD run.sh run.sh
RUN chmod +x run.sh
CMD ./run.sh

