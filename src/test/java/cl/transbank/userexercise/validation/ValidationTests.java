package cl.transbank.userexercise.validation;

import cl.transbank.userexercise.domain.User;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import javax.validation.ConstraintViolation;
import javax.validation.Validation;
import javax.validation.Validator;
import javax.validation.ValidatorFactory;
import java.util.Set;
import java.util.UUID;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class ValidationTests {

    private Validator validator;

    @BeforeEach
    public void setup() {
        ValidatorFactory factory = Validation.buildDefaultValidatorFactory();
        validator = factory.getValidator();
    }

    @Test
    public void shouldFailsWithInvalidEmail() {
        User user = new User();
        user.setName("John");
        user.setUuid(UUID.randomUUID().toString());
        user.setPassword("Af22");
        user.setEmail("invalid_email");
        Set<ConstraintViolation<User>> violations = validator.validate(user);

        assertEquals(violations.size(), 1);
        assertEquals("Email must be correctly formed", violations.iterator().next().getMessage());
    }

    @Test
    public void shouldFailsWithInvalidPassword() {
        User user = new User();
        user.setName("John");
        user.setUuid(UUID.randomUUID().toString());
        user.setPassword("Af2gf");
        user.setEmail("user@server.cl");
        Set<ConstraintViolation<User>> violations = validator.validate(user);

        assertEquals(violations.size(), 1);
        assertEquals("Password must have the format one capital letter, lowercase letters and two numbers", violations.iterator().next().getMessage());
    }

    @Test
    public void shouldBeNoError() {
        User user = new User();
        user.setName("John");
        user.setUuid(UUID.randomUUID().toString());
        user.setPassword("Af2g2f");
        user.setEmail("user@server.cl");
        Set<ConstraintViolation<User>> violations = validator.validate(user);

        assertEquals(violations.size(), 0);
    }
}
