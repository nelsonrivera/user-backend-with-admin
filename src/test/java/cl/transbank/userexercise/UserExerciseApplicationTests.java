package cl.transbank.userexercise;

import cl.transbank.userexercise.controller.UserController;
import cl.transbank.userexercise.domain.User;
import cl.transbank.userexercise.repository.UserRepository;
import cl.transbank.userexercise.service.UserService;
import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.TestPropertySource;
import org.springframework.test.context.junit.jupiter.SpringExtension;
import org.springframework.test.web.servlet.MockMvc;

import static org.hamcrest.Matchers.is;
import static org.mockito.ArgumentMatchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@ExtendWith(SpringExtension.class)
@WebMvcTest(value = UserController.class)
@AutoConfigureMockMvc(addFilters = false)
@TestPropertySource(properties = {"security.enabled=false"})
class UserExerciseApplicationTests {

    @MockBean
    UserService userService;

    @MockBean
    UserRepository userRepository;

    @Autowired
    MockMvc mockMvc;
    final static String USERS_ENDPOINT = "/users";

    @Test
    void shouldReturnBadRequestWithInvalidField() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);

        String jsonPayload = "{\n" +
                "    \"name\": \"juan\",\n" +
                "    \"email\": \"juan@server.cl\",\n" +
                "    \"password\": \"Auy2\"}";

        mockMvc.perform(post(USERS_ENDPOINT).content(jsonPayload)
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isBadRequest())
                .andExpect(jsonPath("$.message", is("password : Password must have the format one capital letter, lowercase letters and two numbers")));

    }

    @Test
    void successCreate() throws Exception {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        objectMapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        objectMapper.enable(DeserializationFeature.ACCEPT_EMPTY_STRING_AS_NULL_OBJECT);

        String jsonPayload = "{\"uuid\":\"879c3528-3b46-4cfb-94e0-b7edc029808d\",\"name\":\"juan\",\"email\":\"juan@server.cl\",\"password\":\"Auy22\"}";

        when(userService.registerUser(any(User.class))).thenReturn(new User());
        mockMvc.perform(post(USERS_ENDPOINT).content(jsonPayload)
                .contentType(MediaType.APPLICATION_JSON)).andExpect(status().isCreated());

    }

}
