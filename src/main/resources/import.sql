
DROP TABLE IF EXISTS user_roles;

CREATE TABLE user_roles ( user_role_id SERIAL,  user_name varchar(100) NOT NULL,  role varchar(100) NOT NULL,  PRIMARY KEY (user_role_id));



INSERT INTO user (uuid,name,email,password,created,modified,last_login,token,isactive) VALUES ('943aec5c-3ce6-11eb-adc1-0242ac120002','admin','admin@server.cl','{bcrypt}$2a$10$lHGiYlGywgHmwXQ/abg0gOvvfYhYl2lqes1Poo9bMePTcVK5KT7W.',null,null,null,'',true);
INSERT INTO phone (id, number, city_code, country_code, user_uuid) values (1, 23455, 44, 56, '943aec5c-3ce6-11eb-adc1-0242ac120002')


INSERT INTO user_roles (user_name, role) VALUES ('admin', 'ROLE_ADMIN');
INSERT INTO user_roles (user_name, role) VALUES ('admin', 'ROLE_USER');


drop table if exists oauth_client_details;
create table oauth_client_details (client_id VARCHAR(255) PRIMARY KEY, resource_ids VARCHAR(255), client_secret VARCHAR(255),  scope VARCHAR(255),  authorized_grant_types VARCHAR(255),  web_server_redirect_uri VARCHAR(255),  authorities VARCHAR(255),   access_token_validity INTEGER,  refresh_token_validity INTEGER,  additional_information VARCHAR(4096),  autoapprove VARCHAR(255));

create table if not exists oauth_client_token (token_id VARCHAR(255),  token LONGVARBINARY,  authentication_id VARCHAR(255) PRIMARY KEY,  user_name VARCHAR(255),  client_id VARCHAR(255));

create table if not exists oauth_access_token ( token_id VARCHAR(255),  token LONGVARBINARY,  authentication_id VARCHAR(255) PRIMARY KEY,  user_name VARCHAR(255),  client_id VARCHAR(255),  authentication LONGVARBINARY, refresh_token VARCHAR(255));

create table if not exists oauth_refresh_token (  token_id VARCHAR(255),  token LONGVARBINARY,  authentication LONGVARBINARY);

create table if not exists oauth_code (  code VARCHAR(255), authentication LONGVARBINARY);

create table if not exists oauth_approvals (	userId VARCHAR(255),	clientId VARCHAR(255),	scope VARCHAR(255),	status VARCHAR(10),	expiresAt TIMESTAMP,	lastModifiedAt TIMESTAMP);

create table if not exists ClientDetails (  appId VARCHAR(255) PRIMARY KEY,  resourceIds VARCHAR(255),  appSecret VARCHAR(255),  scope VARCHAR(255),  grantTypes VARCHAR(255),  redirectUrl VARCHAR(255),  authorities VARCHAR(255),  access_token_validity INTEGER,  refresh_token_validity INTEGER,  additionalInformation VARCHAR(4096),  autoApproveScopes VARCHAR(255));


INSERT INTO oauth_client_details (client_id, client_secret, scope, authorized_grant_types,	web_server_redirect_uri, authorities, access_token_validity,	refresh_token_validity, additional_information, autoapprove) VALUES ('transBankClientId', '{bcrypt}$2a$10$sLQXigvLWuXwlzYT5k9ieOZUL35B6w46Fzw/WHiI4.Y93X2EFKgdu', 'read,write',	'password,authorization_code,refresh_token,client_credentials', 'http://localhost:8082/login', null, 36000, 36000, null, true);
