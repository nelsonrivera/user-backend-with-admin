package cl.transbank.userexercise.exception;

import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.validation.ObjectError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;
import org.springframework.web.context.request.WebRequest;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;

import java.util.List;


@RestControllerAdvice
public class RestResponseEntityExceptionHandler extends ResponseEntityExceptionHandler {


    @ExceptionHandler(value = {UniqueException.class})
    protected ResponseEntity<Object> handleUniqueException(UniqueException ex, WebRequest request) {
        JsonNode exceptionPayload = createExceptionJson(ex.getMessage(), HttpStatus.BAD_REQUEST, request);
        return handleExceptionInternal(ex, exceptionPayload, new HttpHeaders(), HttpStatus.BAD_REQUEST, request);
    }

    @ExceptionHandler(value = {EmptyResultDataAccessException.class})
    protected ResponseEntity<Object> handleEmpty(EmptyResultDataAccessException ex, WebRequest request) {

        return handleExceptionInternal(ex, ex.getMessage(), new HttpHeaders(), HttpStatus.NOT_FOUND, request);
    }

    /**
     * Override
     * MethodArgumentNotValidException: This exception is thrown when argument annotated with @Valid failed validation:
     */
    @Override
    protected ResponseEntity<Object> handleMethodArgumentNotValid(MethodArgumentNotValidException ex,
                                                                  HttpHeaders headers, HttpStatus status, WebRequest request) {

        String message = ex.getMessage();

        List<FieldError> fieldErrors = ex.getBindingResult().getFieldErrors();
        if(fieldErrors != null && !fieldErrors.isEmpty()) {
            message = String.format("%s : %s", fieldErrors.get(0).getField(), fieldErrors.get(0).getDefaultMessage());
        }

        List<ObjectError> globalErrors = ex.getBindingResult().getGlobalErrors();
        if(globalErrors != null && !globalErrors.isEmpty()) {
            message = String.format("%s : %s", globalErrors.get(0).getObjectName(), globalErrors.get(0).getDefaultMessage());
        }

        JsonNode exceptionPayload = createExceptionJson(message.toString(), HttpStatus.BAD_REQUEST, request);
        return handleExceptionInternal(ex, exceptionPayload, headers, HttpStatus.BAD_REQUEST, request);
    }

    private JsonNode createExceptionJson(String message, HttpStatus httpStatus, WebRequest webRequest) {
        JsonNode jsonNode = JsonNodeFactory.instance.objectNode();
        ((ObjectNode) jsonNode).put("message", message);
        return jsonNode;

    }
}
