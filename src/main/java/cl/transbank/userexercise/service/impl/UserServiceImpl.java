package cl.transbank.userexercise.service.impl;

import cl.transbank.userexercise.domain.User;
import cl.transbank.userexercise.exception.UniqueException;
import cl.transbank.userexercise.repository.UserRepository;
import cl.transbank.userexercise.security.OAuth2TokenUtils;
import cl.transbank.userexercise.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.EmptyResultDataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.security.crypto.bcrypt.BCryptPasswordEncoder;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.security.Principal;
import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.StreamSupport;

@Service
public class UserServiceImpl implements UserService {

    UserRepository repository;
    PasswordEncoder passwordEncoder;
    JdbcTemplate jdbcTemplate;

    @Autowired
    OAuth2TokenUtils oAuth2TokenUtils;

    @Autowired
    public UserServiceImpl(UserRepository repository, JdbcTemplate jdbcTemplate) {
        this.repository = repository;
        this.passwordEncoder = new BCryptPasswordEncoder();
        this.jdbcTemplate = jdbcTemplate;
    }

    @Override
    @Transactional
    public User registerUser(User user) {
        checkUniqueEmailAndName(user.getEmail(), user.getName());

        if (user.getPhones() != null && !user.getPhones().isEmpty())
            user.getPhones().forEach(phone -> phone.setUser(user));

        user.setUuid(UUID.randomUUID().toString());
        user.setIsactive(true);
        Date creationDate = new Date();
        user.setCreated(creationDate);
        user.setModified(creationDate);
        user.setLast_login(creationDate);
        String passwordEncoded = passwordEncoder.encode(user.getPassword());
        user.setPassword("{bcrypt}".concat(passwordEncoded));

        System.out.println("Adicionandooooo");
        User saved = repository.save(user);
        System.out.println("Adicionandooooo-5 " + saved.getPhones());

        setAuthoritiesForUser(saved);
        System.out.println("Adicionandooooo1");


        return updateToken(saved);
    }

    @Override
    public User getUser(String id) {
        return repository.findById(id).orElseThrow(() -> new EmptyResultDataAccessException(String.format("User with id %s not found", id), 1));
    }

    @Override
    public User findByEmail(String email) {
        return repository.findFirstByEmail(email).orElseThrow(() -> new EmptyResultDataAccessException(String.format("User with email %s not found", email), 1));
    }

    @Override
    public User login(Principal principal) {
        User user = findByName(principal.getName());
        user.setToken(oAuth2TokenUtils.getCurrentToken());
        user.setLast_login(new Date());
        return repository.save(user);
    }

    @Override
    public List<User> getAllUsers() {
        Iterable<User> all = repository.findAll();

        return StreamSupport.stream(all.spliterator(), false).collect(Collectors.toList());
    }

    public User findByName(String name) {
        return repository.findFirstByName(name).orElseThrow(() -> new EmptyResultDataAccessException(String.format("User with name %s not found", name), 1));
    }

    private User updateToken(User user) {
        OAuth2AccessToken oAuth2AccessToken = oAuth2TokenUtils.generateAccessToken(user.getName());
        user.setToken(oAuth2AccessToken.getValue());
        return repository.save(user);
    }

    private int setAuthoritiesForUser(User user) {
        return jdbcTemplate.update(
                "INSERT INTO user_roles (user_name, role) VALUES (?, ?)", user.getName(), "ROLE_USER");
    }

    private void checkUniqueEmailAndName(String email, String name) {
        Optional<User> firstByEmail = repository.findFirstByEmail(email);
        if (firstByEmail.isPresent()) {
            throw new UniqueException(String.format("The email %s is already configured.", email));
        }

        Optional<User> firstByName = repository.findFirstByName(name);
        if (firstByName.isPresent()) {
            throw new UniqueException(String.format("The name %s is already configured.", name));
        }
    }
}
