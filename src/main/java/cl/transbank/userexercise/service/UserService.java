package cl.transbank.userexercise.service;

import cl.transbank.userexercise.domain.User;

import java.security.Principal;
import java.util.List;

public interface UserService {

    User registerUser(User user);

    User getUser(String id);

    User findByEmail(String email);

    User login(Principal principal);

    List<User> getAllUsers();
}
