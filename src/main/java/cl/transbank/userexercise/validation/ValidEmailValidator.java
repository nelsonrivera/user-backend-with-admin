package cl.transbank.userexercise.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraintvalidation.SupportedValidationTarget;
import javax.validation.constraintvalidation.ValidationTarget;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

@SupportedValidationTarget(ValidationTarget.ANNOTATED_ELEMENT)
public class ValidEmailValidator implements ConstraintValidator<ValidEmail, String> {

    public boolean isValid(String email, ConstraintValidatorContext context) {

        if(email == null)
            return false;
        String regexEmail= "^(.+)@(\\S+)$";
        Pattern patternEmail = Pattern.compile(regexEmail);
        Matcher matcherEmail = patternEmail.matcher(email);
        return matcherEmail.matches();

    }
}
