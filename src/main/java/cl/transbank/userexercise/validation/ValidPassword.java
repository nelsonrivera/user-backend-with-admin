package cl.transbank.userexercise.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = ValidPasswordValidator.class)
@Target({ElementType.FIELD,
        ElementType.ANNOTATION_TYPE,
        ElementType.PARAMETER,
        ElementType.TYPE_USE})

@Retention(RUNTIME)
@Documented
public @interface ValidPassword {

    String message() default "Password must have the format one capital letter, lowercase letters and two numbers";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
