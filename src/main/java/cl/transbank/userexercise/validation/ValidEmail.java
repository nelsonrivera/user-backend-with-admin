package cl.transbank.userexercise.validation;

import javax.validation.Constraint;
import javax.validation.Payload;
import java.lang.annotation.Documented;
import java.lang.annotation.ElementType;
import java.lang.annotation.Retention;
import java.lang.annotation.Target;

import static java.lang.annotation.RetentionPolicy.RUNTIME;

@Constraint(validatedBy = ValidEmailValidator.class)
@Target({ElementType.METHOD, ElementType.FIELD,
        ElementType.ANNOTATION_TYPE,
        ElementType.CONSTRUCTOR,
        ElementType.PARAMETER,
        ElementType.TYPE_USE})

@Retention(RUNTIME)
@Documented
public @interface ValidEmail {

    String message() default "Email must be correctly formed";

    Class<?>[] groups() default {};

    Class<? extends Payload>[] payload() default {};
}
