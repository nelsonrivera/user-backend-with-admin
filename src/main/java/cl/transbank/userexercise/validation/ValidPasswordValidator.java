package cl.transbank.userexercise.validation;

import javax.validation.ConstraintValidator;
import javax.validation.ConstraintValidatorContext;
import javax.validation.constraintvalidation.SupportedValidationTarget;
import javax.validation.constraintvalidation.ValidationTarget;
import java.util.regex.Pattern;

@SupportedValidationTarget(ValidationTarget.ANNOTATED_ELEMENT)
public class ValidPasswordValidator implements ConstraintValidator<ValidPassword, String> {

    public boolean isValid(String password, ConstraintValidatorContext context) {

        if(password == null)
            return false;

        Pattern[] inputRegexes = new Pattern[3];
        inputRegexes[0] = Pattern.compile(".*[A-Z].*");
        inputRegexes[1] = Pattern.compile(".*[a-z].*");
        inputRegexes[2] = Pattern.compile(".*\\d.*\\d.*");

        boolean inputMatches = true;
        for (Pattern inputRegex : inputRegexes) {
            if (!inputRegex.matcher(password).matches()) {
                inputMatches = false;
            }
        }
        return inputMatches;
    }
}
