package cl.transbank.userexercise.controller;

import cl.transbank.userexercise.domain.User;
import cl.transbank.userexercise.service.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.access.prepost.PreAuthorize;
import org.springframework.util.LinkedMultiValueMap;
import org.springframework.util.MultiValueMap;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.security.Principal;
import java.util.List;

@RestController
@RequestMapping("/users")
public class UserController {

    UserService userService;

    private final static Logger LOGGER = LoggerFactory.getLogger(UserController.class);

    @Autowired
    public UserController(UserService userService) {
        this.userService = userService;
    }

    @PostMapping(consumes = "application/json")
    public ResponseEntity<User> registerUser(@RequestBody @Valid User user) {

        LOGGER.info("Received request to register user: '{}'", user.getName());
        User registered = userService.registerUser(user);

        MultiValueMap<String, String> multiValueMap = new LinkedMultiValueMap<>();
        multiValueMap.add("location", "/users/" + registered.getUuid());

        LOGGER.info("Created user: '{}'", user.getName());
        return new ResponseEntity<>(registered, multiValueMap, HttpStatus.CREATED);
    }

    @GetMapping(value = "/{id}")
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<User> getUser(@PathVariable String id) {

        User user = userService.getUser(id);
        return ResponseEntity.ok(user);
    }

    @GetMapping
    @PreAuthorize("hasAuthority('ROLE_ADMIN')")
    public ResponseEntity<List<User>> getAllUser() {

        List<User> users = userService.getAllUsers();
        return ResponseEntity.ok(users);
    }

    @GetMapping(value = "/login")
    public ResponseEntity<User> showLoginUser(Principal principal) {

        User user = userService.login(principal);

        LOGGER.info("Login by user '{}' at '{}'", user.getName(), user.getLast_login());
        return ResponseEntity.ok(user);
    }
}
