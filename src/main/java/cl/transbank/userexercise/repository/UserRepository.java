package cl.transbank.userexercise.repository;

import cl.transbank.userexercise.domain.User;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

import java.util.Optional;

@Repository
public interface UserRepository extends PagingAndSortingRepository<User, String> {

    Optional<User> findFirstByEmail(String email);

    Optional<User> findFirstByName(String name);
}
