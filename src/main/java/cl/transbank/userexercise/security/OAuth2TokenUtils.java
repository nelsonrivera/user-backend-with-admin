package cl.transbank.userexercise.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.boot.autoconfigure.condition.ConditionalOnProperty;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.oauth2.client.OAuth2ClientContext;
import org.springframework.security.oauth2.common.OAuth2AccessToken;
import org.springframework.security.oauth2.provider.ClientDetailsService;
import org.springframework.security.oauth2.provider.OAuth2Authentication;
import org.springframework.security.oauth2.provider.OAuth2Request;
import org.springframework.security.oauth2.provider.token.DefaultTokenServices;
import org.springframework.security.oauth2.provider.token.store.JwtAccessTokenConverter;
import org.springframework.stereotype.Component;

import java.util.*;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Component
@ConditionalOnProperty(value = "security.enabled", matchIfMissing = true)
public class OAuth2TokenUtils {

    @Autowired
    ClientDetailsService clientDetailsService;

    @Autowired
    JwtAccessTokenConverter accessTokenConverter;

    @Autowired
    DefaultTokenServices tokenServices;

    @Autowired
    UserDetailsService userDetailsService;

    @Autowired
    OAuth2ClientContext oauth2ClientContext;

    @Value("${security.oauth2.client.client-id}")
    String clientId;

    @Value("${security.oauth2.client.scope}")
    Set scope;

    public OAuth2TokenUtils() {
    }

    public OAuth2AccessToken generateAccessToken(String userName) {
        UserDetails userDetails = userDetailsService.loadUserByUsername(userName);

        tokenServices.setTokenEnhancer(accessTokenConverter);
        tokenServices.setClientDetailsService(clientDetailsService);
        OAuth2AccessToken oauth2Token = tokenServices.createAccessToken(getAuthentication(userDetails));
        return oauth2Token;
    }

    private OAuth2Authentication getAuthentication(UserDetails userDetails) {

        Map<String, String> parameters = Stream.of(
                new AbstractMap.SimpleEntry<>("grant_type", "password"),
                new AbstractMap.SimpleEntry<>("username", userDetails.getUsername()))
                .collect(Collectors.toMap(Map.Entry::getKey, Map.Entry::getValue));


        OAuth2Request request = new OAuth2Request(parameters, clientId, null, true, scope, null, null, null, null);
        return new OAuth2Authentication(request, new UsernamePasswordAuthenticationToken(userDetails, null, userDetails.getAuthorities()));
    }

    public String getCurrentToken() {
        return oauth2ClientContext.getAccessToken().getValue();
    }
}
