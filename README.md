# README #


### ¿Qué contiene este repositorio? ###

* Este repositorio contiene un proyecto para administrar usuarios. La aplicación consiste de un API REST para la gestión de usuarios.

### Uso y Configuración ###

* Tecnologías usadas

     * El API REST está protegido usando Oauth2 con formato JWT de los tokens de acceso. 
   
     * Está construido usando Gradle, Java, Spring Security, Spring Web, Spring Cloud, Spring Data Jpa con Hibernate, Log LogBack.
  
     * Base de datos H2 en memoria.
   
* Instrucciones para el despliegue
  
      * Puede correr las pruebas unitarias usando : gradle test
  
      * Construya la aplicación usando: gradle build
   
   La aplicación está construida usando Sprint Boot, use el jar generado en la carpeta build/libs/ para levantar la aplicación:
   
   java -jar user-backend-management-with-admin-1.0.jar
   
* Google Cloud Platform

 La aplicación está disponible además en GCP desplegada en un cluster Kubernetes, puede acceder con la URL:
 
 http://35.199.92.43/
   
  
  Endpoints:
  
   * Obtener token : 
      * POST: /oauth/token. 
      
      Endpoint para obtener un token JWT válido para acceder a los endpoints del API. Recibe los parámetros clientId, clientSecret, usuario, password, grant_type.
  
   * Registrar usuario: 
      * POST: /users. 
      
       Endpoint para crear un usuario. Este endpoint no está protegido por OAUTH2 y puede ser consumido de forma libre por un usuario para registrarse. Al usuario creado se le asigna el rol 'USER'. Es retornado la información del usuario creado. Además del token de acceso JWT asociado al usuario creado, el cual el usuario puede usar para acceder al endpoint de login.
       
       Además con el nuevo usuario creado se puede obtener un token JWT usando el endpoint de obtener token. Note que el token obtenido tendrá el rol 'USER'.
       
       Trazas de log del usuario creado.
       
   * Login:
   
     * GET: /login.
     
       Este endpoint solo devuelve la información del usuario que accedió. Está protegido por OAUTH2 y puede acceder un usuario válido con cualquier rol. Las peticiones de login deben contener una cabecera con un token autorizado, en formato Authorization: Bearer <token>. Es actualizado automaticamante el campo last_login del usuario, con la fecha del último login asociado.
       
       Trazas de log del usuario logueado.
       
   * Get All User:
      
     * GET: /users.
        
       Este endpoint retorna todos los usuarios registrados. Está protegido por OAUTH2 y está protegido por OAUTH2 y solo puede ser accedido por un usuario válido con rol de 'ADMIN'. Las peticiones deben contener una cabecera con un token autorizado, en formato Authorization: Bearer <token>.
          
       Trazas de log.


* Configuración inicial
    
    Al iniciar la aplicación automaticamante se crea un usuario:admin con password:password1 con rol 'ADMIN' mediante el archivo import.sql, que puede servir de prueba. Además se inicia una aplicación OAUH2 con clientID=transBankClientId y clientSecret=secret, estos son los datos que deben usarse para obtener un token usando el endpoint de obtener token.
    
    En el proyecto se incluye una colleción postman con peticiones útiles para probar la aplicación. 
   
       


### Contacto ###

* nelsonrivera12@gmail.com
